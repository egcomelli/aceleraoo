using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Acelera.OO.CarRental.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CalculadorDeveImprimirRecibo()
        {
            Veiculo<IAdicional> veiculo = new Carro();
            veiculo.Adicionais.Add(new GpsPequeno());
            //veiculo.Adicionais.Add(new Adicional { Descricao = "Geladeira", Valor = 250 });
            Reserva reserva = new Reserva();
            reserva.Veiculo = veiculo;
            reserva.Inicio = DateTime.Now.AddDays(-2);
            reserva.Fim = DateTime.Now;
            reserva.EstimativaKm = 850;
            Calculador calculador = new Calculador();
            var resultado = calculador.Calcula(reserva);
        }
    }
}
