﻿namespace Acelera.OO.CarRental
{
    public class MotorHome : Veiculo<IMotorHomeAdicional>
    {
        public MotorHome() : base(EnumTipoVeiculo.MotorHome, 300, 0.65m){}

    }
}
