﻿using System.Collections.Generic;

namespace Acelera.OO.CarRental
{
    public abstract class Veiculo<ITipoAdicional>
        where ITipoAdicional : IAdicional
    {
        public List<ITipoAdicional> Adicionais { get; }
        public EnumTipoVeiculo TipoVeiculo { get; }
        public decimal ValorDiaria { get; }
        public decimal PrecoKm { get; }

        protected Veiculo(EnumTipoVeiculo tipoVeiculo, decimal valorDiaria, decimal precoKm)
        {
            TipoVeiculo = tipoVeiculo;
            Adicionais = new List<ITipoAdicional>();
            ValorDiaria = valorDiaria;
            PrecoKm = precoKm;
        }

    }
}