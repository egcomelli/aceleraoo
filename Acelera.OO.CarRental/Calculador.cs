﻿using System.Text;

namespace Acelera.OO.CarRental
{
    public class Calculador
    {
        public string Calcula(Reserva reserva)
        {
            var estimativaKmReais = reserva.Veiculo.PrecoKm * reserva.EstimativaKm;
            var retorno = new StringBuilder();
            retorno.AppendLine($"Tipo do Carro: {reserva.Veiculo.TipoVeiculo}");
            retorno.AppendLine($"Quantidade de diárias: {reserva.CalcularNumeroDiarias()}");
            retorno.AppendLine($"Valor total das diárias: {CalculaValorTotalDiarias(reserva)}");
            retorno.AppendLine($"Estimativa de quilometragem em reais: {estimativaKmReais}");
            retorno.Append("Valores de todos os adicionais: ");
            var somaAdicionais = 0;
            foreach (var adicional in reserva.Veiculo.Adicionais)
            {
                retorno.Append($"{adicional.Descricao}: R$ {adicional.Valor} ");
                somaAdicionais += adicional.Valor;
            }
            retorno.AppendLine($"Valor total do aluguel: R$ {somaAdicionais + estimativaKmReais + CalculaValorTotalDiarias(reserva)}");

            return retorno.ToString();
        }

        private decimal CalculaValorTotalDiarias(Reserva reserva)
        {
            return reserva.CalcularNumeroDiarias() * reserva.Veiculo.ValorDiaria;
        }

        //private decimal CalculaValorTotalDiaria()

    }
}
