﻿namespace Acelera.OO.CarRental
{

    public abstract class Adicional : IAdicional
    {
        public int Valor { get;}
        public string Descricao { get; }

        protected Adicional(int valor, string descricao)
        {
            Valor = valor;
            Descricao = descricao;
        }
    }
}
