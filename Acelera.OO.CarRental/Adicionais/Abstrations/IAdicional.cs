﻿namespace Acelera.OO.CarRental
{
    public interface IAdicional
    {
        int Valor { get; }
        string Descricao { get;}
    }
}
