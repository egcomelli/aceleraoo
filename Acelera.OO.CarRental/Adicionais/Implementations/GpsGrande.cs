﻿namespace Acelera.OO.CarRental.Adicionais.Implementations
{
    public class GpsGrande : Adicional, IMotorHomeAdicional
    {
        public GpsGrande() : base(35, "GPS Grande")
        {
        }
    }
}
