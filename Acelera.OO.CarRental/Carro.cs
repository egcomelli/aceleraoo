﻿namespace Acelera.OO.CarRental
{
    public class Carro : Veiculo<ICarroAdicional>
    {
        public Carro() : base(EnumTipoVeiculo.Carro, 50, 0.5m){}
    }
}
