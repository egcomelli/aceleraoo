﻿using System;

namespace Acelera.OO.CarRental
{
    public class Reserva
    {
        public Veiculo<IAdicional> Veiculo { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public decimal EstimativaKm { get; set; }

        public int CalcularNumeroDiarias()
        {
            return (Fim - Inicio).Days;
        }

    }
}
